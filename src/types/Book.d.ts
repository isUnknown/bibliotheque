export type Book = {
  authorName: string
  authorSurname: string
  title: string
  publicationYear?: string
  type?: string
  tags?: Array
  publisher?: string
  editionPlace?: string
  editionYear?: string
  collection?: string
  isEditing?: boolean
  loan?: string
  created?: string
}
