import { ref, computed } from "vue"
import { Book } from "../types/Book"

const useInteractiveBooks = ref<Book[]>([])

const useStaticBooks = computed(() => {
  return useInteractiveBooks.value.map((book) => {
    const { isEditing, ...bookWithoutIsEditing } = book
    return bookWithoutIsEditing
  })
})

const useEditingBook = computed<Book | false>(
  () => useInteractiveBooks.value.find((book) => book?.isEditing) || false
)

const useCategories = [
  {
    text: "titre",
    value: "title",
    main: true,
  },
  {
    text: "prénom",
    value: "authorSurname",
    main: true,
  },
  {
    text: "nom",
    value: "authorName",
    main: true,
  },
  {
    text: "année de publication",
    value: "publicationYear",
    main: true,
  },
  {
    text: "genre",
    value: "type",
    main: true,
  },
  {
    text: "étiquettes",
    value: "tags",
    main: true,
  },
  {
    text: "édition",
    value: "publisher",
    main: false,
  },
  {
    text: "lieu d'édition",
    value: "editionPlace",
    main: false,
  },
  {
    text: "année d'édition",
    value: "editionYear",
    main: false,
  },
  {
    text: "collection",
    value: "collection",
    main: false,
  },
  {
    text: "prêté",
    value: "loan",
    main: false,
  },
  {
    text: "date d'ajout",
    value: "created",
    main: false,
  },
]

function saveBooks() {
  return new Promise<void>((resolve, reject) => {
    const init = {
      method: "POST",
      body: JSON.stringify(useStaticBooks.value),
    }
    fetch("http://192.168.1.12:8888/scripts/saveBooks.php", init)
      .then((res) => res.json())
      .then((json) => {
        resolve(json)
      })
      .catch((err) => {
        reject(err)
      })
  })
}

function backupBooks() {
  return new Promise<void>((resolve, reject) => {
    const init = {
      method: "POST",
      body: JSON.stringify(useStaticBooks.value),
    }
    fetch("http://192.168.1.12:8888/scripts/backupBooks.php", init)
      .then((res) => res.json())
      .then((json) => {
        resolve(json)
      })
      .catch((err) => {
        reject(err)
      })
  })
}

function removeBook(bookToRemove: Book) {
  if (
    bookToRemove.title.length > 0 ||
    bookToRemove.authorName.length > 0 ||
    bookToRemove.authorSurname.length > 0
  ) {
    if (confirm("Supprimer le livre ?")) {
      useInteractiveBooks.value = useInteractiveBooks.value.filter(
        (book) => book.title !== bookToRemove.title
      )
    }
  } else {
    useInteractiveBooks.value = useInteractiveBooks.value.filter(
      (book) => book.title !== bookToRemove.title
    )
  }
  saveBooks()
}

function resetBook(book: Book) {
  const correspondingServerBook = useServerBooks.value.find(
    (serverBook) => serverBook.title === book.title
  )

  if (correspondingServerBook) {
    useInteractiveBooks.value = useInteractiveBooks.value.map(
      (interactiveBook) => {
        if (interactiveBook.title === correspondingServerBook.title) {
          const updatedBook = Object.assign({}, correspondingServerBook)
          updatedBook.isEditing = false
          return updatedBook
        }
        return interactiveBook
      }
    )
  } else {
    removeBook(book)
  }
}

function addNewBook() {
  const date = new Date(Date.now())
  const year = date.getFullYear()
  const month = String(date.getMonth() + 1).padStart(2, "0")
  const day = String(date.getDate()).padStart(2, "0")
  const created = `${year}-${month}-${day}`
  const newBook: Book = {
    title: "",
    authorSurname: "",
    authorName: "",
    publicationYear: "",
    type: "",
    tags: "",
    publisher: "",
    editionPlace: "",
    editionYear: "",
    collection: "",
    isEditing: true,
    loan: "",
    created: created,
  }
  useInteractiveBooks.value.push(newBook)
}

export {
  useInteractiveBooks,
  useEditingBook,
  useCategories,
  useStaticBooks,
  resetBook,
  addNewBook,
  removeBook,
  saveBooks,
  backupBooks,
}
